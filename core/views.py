from django.shortcuts import render
from .models import B2file

def index(request):
    files = B2file.objects.all()
    return render(request, "index.html", {"files":files})
