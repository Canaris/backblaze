from django.db import models

class B2file(models.Model):

	title = models.CharField(max_length=30)
	file = models.FileField()

	class Meta:
		verbose_name = "backblaze File Upload"
		verbose_name_plural = "backblaze File Upload"

	def __str__(self):
		return self.title
